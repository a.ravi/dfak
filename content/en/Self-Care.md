---
layout: page
title: "Feeling Overwhelmed?"
author: FP
language: en
summary: "Feeling Overwhelmed?"
date: 2018-09-05
permalink: /en/Self-Care/
parent: Home
---

# Feeling Overwhelmed?

Online harassment, threats, and other kinds of digital attacks can create overwhelming feelings and very delicate emotional states: you might feel guilty, ashamed, anxious, angry, confused, helpless, or even fear for your psychological or physical well-being.

There is no "right" way to feel, as your state of vulnerability and what your personal information means to you is different from person to person. Any emotion is justified, and you shouldn't worry about whether or not your reaction was the right one.

The first thing you should remember is that what is happening to you is not your fault and you should not blame yourself, but possibly reach out to someone trusted who can support you in addressing this emergency.

To mitigate an online attack, you will need to gather information on what has happened, but you don't have to do it on your own - if you have a person you trust, you can ask them to support you while following the instructions in this website, or give them access to your devices or accounts to gather information for you.

Read more about how to protect yourself against overwhelming feelings:

- [Self-Care for Activists: Sustaining Your Most Valuable Resource](https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource)
- [Caring for yourself so you can keep defending human rights](https://www.amnesty.org.au/activism-self-care/)
