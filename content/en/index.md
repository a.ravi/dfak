---
layout: page
title: "Introduction"
author: RaReNet
language: en
summary: "The Digital First Aid Kit aims to provide preliminary support for people facing the most common types of digital threats. The Kit offers a set of self-diagnostic tools for human rights defenders, bloggers, activists, journalists, and their communities facing attacks themselves, as well as providing guidelines for digital first responders to assist a person under threat."
date: 2015-08
permalink: /en/
parent: Home
---
# Welcome to the Digital First Aid Kit!

The Digital First Aid Kit (DFAK) is a free resource to help human rights defenders, bloggers, activists, and journalists better protect themselves and their communities against the most common types of digital threats. If you or someone you are assisting has a digital safety need, the DFAK will guide you in diagnosing the issues and refer you to support providers for further help.   

## Who is DFAK for?

The DFAK can assist you in navigating the resources and organizations that exist in the rapid response community to provide support to at-risk communities.

## How to use DFAK?

Starting with defining the problem, the DFAK walks you through a set of questions to better define the cause of problem and collect important information to resolve the problem, followed by recommendations of useful resources. For problems you cannot tackle on your own, the DFAK has a list of support organizations for you to consult with. 

First, describe the problem you are tackling:

- I lost control of my device
- I lost access to my accounts
- I may be under phishing or malware attack 
- My website is not working
- Someone is impersonating me online
- I am being harassed online 

## Who is behind DFAK?

DFAK is a collaborative effort of the [Rapid Response Network](https://rarenet.org), which includes organizations such as EFF, Global Voices, Hivos & the Digital Defenders Partnership, Front Line Defenders, Internews, Freedom House, Access Now, Virtual Road, CIRCL, Open Technology Fund, as well as individual security experts who are working in the field of digital security and rapid response. It is a work in progress and we [welcome any comments, suggestions, or questions](https://gitlab.com/rarenet/dfak).

## What else you need to know?

*DFAK is not meant to serve as the ultimate solution to all your digital emergencies.* If you feel uncertain about how to address the problem or implement a solution in helping others troubleshoot using DFAK, feel free to ask for help from [trained professionals](https://www.dfak.org/list-of-experts) in [secure ways](https://dfak.org/securecomms). You can also refer to [this list of glossary](url to the glossary) to clarify terms. 

## Feeling overwhelmed?

Connect directly with an organization that can provide initial support for you to navigate your needs and this resource.